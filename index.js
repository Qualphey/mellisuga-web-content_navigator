
const page_display_limit = 5;


const XHR = require('web/utils/xhr_async.js');

const html = require("./body.html");


const Page = require("./page.js");

module.exports = class ContentNavigator {
  constructor(cfg, navi_cb) {
    this.element = document.createElement("div");
    this.element.innerHTML = html;
    if (cfg.div) {
      cfg.div.appendChild(this.element);
    } else {
      document.body.appendChild(this.element);
    }
    this.display = this.element.querySelector(".contnav_display");

    this.navi_cb = navi_cb;

    let max_index = this.max_index = cfg.max_index; 
    let page_size = this.page_size = cfg.page_size || 16;

    let max_pages = this.max_pages = Math.ceil((max_index)/page_size);

    if (max_pages < 1) {
      max_pages = 1;
      this.max_pages = 1;
    }

    this.pages = [];

    let buttons = this.buttons = [];
   
    let navilements = this.element.querySelectorAll(".contnav_controls");

    let controls = this.controls = {
      top: navilements[0],
      bottom: navilements[1]
    };

    let this_class = this;

    this.prev_func = async function(ev) {
      try {
        const next_index = this_class.cur_page.index-1;
        if (next_index >= 0) {
          await this_class.select_page(next_index);
        }
      } catch (e) {
        console.log(e.stack);
      }
    }

    this.next_func = async function(ev) {
      try {
        const next_index = this_class.cur_page.index+1;
        if (next_index < this_class.max_pages) {
          await this_class.select_page(next_index);
        }
      } catch (e) {
        console.log(e.stack);
      }
    }

    this.controls.top.preve = controls.top.querySelector('.prev');
    this.controls.top.preve.addEventListener("click", this.prev_func);
    this.controls.bottom.preve = controls.bottom.querySelector('.prev');
    this.controls.bottom.preve.addEventListener("click", this.prev_func);

    this.controls.top.nexte = controls.top.querySelector('.next');
    this.controls.top.nexte.addEventListener("click", this.next_func);
    this.controls.bottom.nexte = controls.bottom.querySelector('.next');
    this.controls.bottom.nexte.addEventListener("click", this.next_func); 
    
    console.log("max_pages", max_pages);

    for (let p = 0; p < max_pages; p++) {
      if (max_index%page_size > 0) {

      }
      let button = {
        index: p,
        top: document.createElement('button'),
        bottom: document.createElement('button'),
        func: async function() {
          try {
            await this_class.select_page(p);
          } catch (e) {
            console.error(e.stack);
          }
        }
      };

      button.top.addEventListener("mouseover", button.drag_over);

      buttons.push(button);

    }
    
    for (let p = 0; p < this.buttons.length; p++) {
      let button = this.buttons[p];
      button.top.innerHTML = p+1; button.bottom.innerHTML = p+1;
      button.top.addEventListener('click', button.func);
      button.bottom.addEventListener('click', button.func);

      controls.top.insertBefore(button.top, this.controls.top.nexte);
      controls.bottom.insertBefore(button.bottom, this.controls.bottom.nexte);
    }

  }

  static async init(cfg, navi_cb) {
    try {

      let this_class = new module.exports(cfg, navi_cb);

      return this_class;

    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async select_page(p) {
    try {
      display_loading_overlay()
      if (this.cur_page && this.buttons[this.cur_page.index]) {
        this.buttons[this.cur_page.index].top.classList.remove("selected");
        this.buttons[this.cur_page.index].bottom.classList.remove("selected");
      } 

      await this.get_page(p, this.page_size);
    } catch (e) {
      console.error(e.stack);
    }
  }
  
  async get_page(index, page_size) {
    try {
      console.log("this.buttons", this.buttons);
      this.selected_button = this.buttons[index];
      console.log("this.selected_button", this.selected_button);

      if (this.cur_page) {
        this.cur_page.destroy();
        delete this.cur_page;
      }

      
      this.cur_page = await Page.init(this, index, page_size); 
      console.log("set cur page", this.cur_page);
      this.pages[index] = this.cur_page;
      if (this.cur_page.grid_ui) {
        await this.cur_page.resize(this.cur_page.grid_ui.items_in_row); 
        this.cur_page.listen_resize();
        await this.cur_page.resize_listener();
      }

      console.log("cur page this", this.cur_page.callback);
      if (this.cur_page) await this.cur_page.callback();
            
      hide_loading_overlay()
      return this.cur_page;
    } catch(e) {
      console.error(e.stack);
      return undefined;
    }
  }

  start_end(new_max_pages, page_limit) {
    let steps_back = Math.floor(page_limit/2);

    let starti = 0;
    if (this.selected_button) {
      starti = this.selected_button.index-steps_back;
    }

    if (starti < 0) starti = 0;          
    let endi = starti + page_limit;


    if (endi > new_max_pages) {
      endi = new_max_pages;
    }

    if (starti + page_limit > endi) {
      starti = endi - page_limit;
      if (starti < 0) starti = 0;
    }

    console.log("starti", starti);
    console.log("endi", endi);
    
    return { start: starti, end: endi };
  }

  async resize(page_size, max_index) {
    try {
      let this_class = this;
      if (!page_size) page_size = this.page_size; 
      this.page_size = page_size;
      this.max_index = max_index || this.max_index;
      let new_max_pages = Math.ceil((this.max_index)/page_size);

//      console.log('new page size', page_size);
//      console.log('new_max_pages', new_max_pages);
//      console.log('this.max_pages', this.max_pages);
      
      console.log("resize" ,page_size, max_index);

      let display_limit = page_display_limit;
      if (display_limit > new_max_pages) {
        display_limit = new_max_pages;
      }

      let page_limit = page_display_limit;
console.log("cur_page", this.cur_page);
      if (this.cur_page) {
        if (this.cur_page.index == 0) {
          if (this.controls.top.contains(this.controls.top.preve)) {
            this.controls.top.removeChild(this.controls.top.preve);
            this.controls.bottom.removeChild(this.controls.bottom.preve);
          }
          page_limit++;
        } else if (this.cur_page.index != 0 && !this.controls.top.contains(this.controls.top.preve)) {
          this.controls.top.insertBefore(this.controls.top.preve, this.controls.top.firstChild);
          this.controls.bottom.insertBefore(this.controls.bottom.preve, this.controls.bottom.firstChild);
        }

        if (this.cur_page.index == new_max_pages-1) {
          if (this.controls.top.contains(this.controls.top.nexte)) {
            this.controls.top.removeChild(this.controls.top.nexte);
            this.controls.bottom.removeChild(this.controls.bottom.nexte);
          }
          page_limit++;
        } else if (this.cur_page.index != new_max_pages-1 && !this.controls.top.contains(this.controls.top.nexte)) {
          this.controls.top.appendChild(this.controls.top.nexte);
          this.controls.bottom.appendChild(this.controls.bottom.nexte);
        }
      }

      if (new_max_pages < 2) {
        if (this.controls.top.contains(this.controls.top.nexte)) {
          this.controls.top.removeChild(this.controls.top.nexte);
          this.controls.bottom.removeChild(this.controls.bottom.nexte);
        }
      }

      let start_end = this.start_end(new_max_pages, page_limit);
      let starti = start_end.start, endi = start_end.end;

      if (starti == 0 || endi == new_max_pages) page_limit++;

      start_end = this.start_end(new_max_pages, page_limit);
      starti = start_end.start;
      endi = start_end.end;

//      console.log("starti", starti);
//      console.log("endi", endi);
      
      for (let b = 0; b < this.max_pages; b++) {
        if (this.buttons[b] && this.controls.top.contains(this.buttons[b].top)) {
          this.buttons[b].top.removeEventListener('click', this.buttons[b].func);
          this.buttons[b].bottom.removeEventListener('click', this.buttons[b].func);
          this.controls.top.removeChild(this.buttons[b].top);
          this.controls.bottom.removeChild(this.buttons[b].bottom);
        }
      }
      this.buttons = [];

      for (let b = 0; b < new_max_pages; b++) {
        let p = this.buttons.length;

        let button = {
          index: p,
          top: document.createElement('button'),
          bottom: document.createElement('button'),
          func: async function() {
            try {
              await this_class.select_page(p);
            } catch (e) {
              console.error(e.stack);
            }
          }
        };
        button.top.innerHTML = p+1; button.bottom.innerHTML = p+1;
        button.top.addEventListener('click', button.func);
        button.bottom.addEventListener('click', button.func);
        this.buttons.push(button);
      }

      if (new_max_pages > 1) {
        for (let b = starti; b < endi; b++) {
          if (!this.controls.top.contains(this.buttons[b].top)) {
            if (this.controls.top.contains(this.controls.top.nexte)) {
              this.controls.top.insertBefore(this.buttons[b].top, this.controls.top.nexte);
              this.controls.bottom.insertBefore(this.buttons[b].bottom, this.controls.bottom.nexte);
            } else {
              this.controls.top.appendChild(this.buttons[b].top);
              this.controls.bottom.appendChild(this.buttons[b].bottom);
            }
            if (this.selected_button.index == b) {
              this.buttons[b].top.classList.add("selected");
              this.buttons[b].bottom.classList.add("selected");
            }
          }
        }
      }

      if (starti > 0) {
        if (!this.controls.top.contains(this.buttons[0].top)) {
          this.controls.top.insertBefore(this.buttons[0].top, this.controls.top.firstChild);
          this.controls.bottom.insertBefore(this.buttons[0].bottom, this.controls.bottom.firstChild);
        }
      }

      if (endi < new_max_pages) {
        if (!this.controls.top.contains(this.buttons[new_max_pages-1].top)) {
          this.controls.top.appendChild(this.buttons[new_max_pages-1].top);
          this.controls.bottom.appendChild(this.buttons[new_max_pages-1].bottom);
        }
      }

      if (new_max_pages < this.max_pages && this.cur_page && this.cur_page.index >= new_max_pages) {
        let p = new_max_pages-1;
//        await this.get_page(p, this.page_size);
      }
      this.max_pages = new_max_pages;

    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  drag_mode() {
    this.mode = "drag";
    this.drag_controls = {
      top: document.createElement('div'),
      bottom: document.createElement('div')
    };
    this.drag_controls.top.classList.add("drag_page_controls");
    this.drag_controls.bottom.classList.add("drag_page_controls");

    this.element.replaceChild(this.drag_controls.top, this.controls.top);
    this.element.replaceChild(this.drag_controls.bottom, this.controls.bottom);
    
    let start_end = this.start_end(this.max_pages, 5);
    
    let starti = start_end.start, endi = start_end.end;

    this.starti = starti;

    this.drag_buttons = [];
    let this_class = this;

    if (this.max_pages > 1) {
      this.drag_controls.top.classList.remove("no_pages");
      this.drag_controls.bottom.classList.remove("no_pages");
      for (let b = starti; b < endi; b++) {
        let button = {
          index: b,
          top: document.createElement('button'),
          bottom: document.createElement('button'),
          func: async function() {
            try {
              await this_class.select_page(p);
            } catch (e) {
              console.error(e.stack);
            }
          }
        };
        button.top.innerHTML = b+1; button.bottom.innerHTML = b+1;
        button.top.addEventListener('click', button.func);
        button.bottom.addEventListener('click', button.func);
        if (b == this.cur_page.index) {
          button.top.classList.add("selected");
          button.bottom.classList.add("selected");
        }
        this.drag_controls.top.appendChild(button.top);
        this.drag_controls.bottom.appendChild(button.bottom);
        this.drag_buttons.push(button);
      } 
    } else {
      this.drag_controls.top.classList.add("no_pages");
      this.drag_controls.bottom.classList.add("no_pages");
    }
  }

  normal_mode() {

    this.element.replaceChild(this.controls.top, this.drag_controls.top);
    this.element.replaceChild(this.controls.bottom, this.drag_controls.bottom);
  }

  roll_left() {
    let b = this.drag_buttons[0].index-1;

    if (b >= 0) {
      let last_button = this.drag_buttons[this.drag_buttons.length-1];
      this.drag_controls.top.removeChild(last_button.top);
      this.drag_controls.bottom.removeChild(last_button.bottom);
      this.drag_buttons.pop();


      let button = {
        index: b,
        top: document.createElement('button'),
        bottom: document.createElement('button'),
        func: async function() {
          try {
            await this_class.select_page(p);
          } catch (e) {
            console.error(e.stack);
          }
        }
      };
      button.top.innerHTML = b+1; button.bottom.innerHTML = b+1;
      button.top.addEventListener('click', button.func);
      button.bottom.addEventListener('click', button.func);
      if (b == this.cur_page.index) {
        button.top.classList.add("selected");
        button.bottom.classList.add("selected");
      }
      this.drag_controls.top.insertBefore(button.top, this.drag_buttons[0].top);
      this.drag_controls.bottom.insertBefore(button.bottom, this.drag_buttons[0].bottom);
      this.drag_buttons.splice(0, 0, button);
    }

  }

  roll_right() {
    let b = this.drag_buttons[this.drag_buttons.length-1].index+1;

    if (b < this.max_pages) {
      let first_button = this.drag_buttons[0];
      this.drag_controls.top.removeChild(first_button.top);
      this.drag_controls.bottom.removeChild(first_button.bottom);
      this.drag_buttons.splice(0, 1);

      let button = {
        index: b,
        top: document.createElement('button'),
        bottom: document.createElement('button'),
        func: async function() {
          try {
            await this_class.select_page(p);
          } catch (e) {
            console.error(e.stack);
          }
        }
      };
      button.top.innerHTML = b+1; button.bottom.innerHTML = b+1;
      button.top.addEventListener('click', button.func);
      button.bottom.addEventListener('click', button.func);
      if (b == this.cur_page.index) {
        button.top.classList.add("selected");
        button.bottom.classList.add("selected");
      }
      this.drag_buttons.push(button);
      this.drag_controls.top.appendChild(button.top);
      this.drag_controls.bottom.appendChild(button.bottom);
    }
  }

  pageByIndex(index) {
    return null;
  }
}


