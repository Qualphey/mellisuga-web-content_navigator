

module.exports = class {
  constructor(navigator, index, client_page) {
    this.client_page = client_page;
    this.index = index;
    this.element = document.createElement('div');
    navigator.display.appendChild(this.element);

    this.navigator = navigator;
  }

  static async init(navigator, index, page_size) { 
    try {
      let client_page = null; //await navigator.navi_cb(index, page_size, navigator.max_pages, navigator);
      let this_class = new module.exports(navigator, index, client_page);
      return this_class;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async callback() {
    this.client_page = await this.navigator.navi_cb(this.index, this.navigator.page_size, this.navigator.max_pages, this.navigator);
  }
 
  async resize() {
    if (this.client_page) await this.client_page.resize();
  }

  destroy() {
    console.log("destroy");
    this.navigator.display.innerHTML = "";
  }
}
